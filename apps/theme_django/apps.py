from django.apps import AppConfig


class Theme_djangoConfig(AppConfig):
    name = 'apps.theme_django'
