from tabnanny import verbose
from django.db import models


class Category(models.Model):
    name = models.CharField(
        'category name',
        max_length = 100,
        null = False,
        blank = False
    )

    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'
    

    def __str__(self) -> str:
        return self.name


class Article(models.Model):
    name = models.CharField(
        'Article name',
        max_length = 100,
        null = False,
        blank = False
    )
    summary = models.CharField(
        'article description',
        max_length=150,
        null = False,
        blank = False
    )
    content = models.CharField(
        'Content',
        max_length=200,
        null = False,
        blank = False
    )
    categories = models.ForeignKey(
        Category,
        on_delete = models.CASCADE
    )
    public = models.BooleanField(
        default = True
    )
    date_creation = models.DateTimeField(
        'Publish date',
        auto_now_add = True
    )
    date_edit = models.DateTimeField(
        'Edit date',
        auto_now_add = True
    )
    slug = models.SlugField(
        'Slug',
        max_length=100,
        null=True,
        blank=True,
        unique=True,
    )

    class Meta:
        verbose_name = 'Article'
        verbose_name_plural = 'Articles'


    def __str__(self) -> str:
        return self.name