from multiprocessing import context
from django.shortcuts import render
from apps.blog.models import Article


def index(request):
    articles = Article.objects.filter(public=True)
    context = {
        'name': 'Daniel',
        'last_name': 'Ticona',
        'articles': articles,
    }

    return render(request, 'blog/index.html', context)

def article_detail(request, slug):
    try:
        article = Article.objects.get(slug=slug)
    except:
        article = None
    context = {
        'article': article
    }
    if article != None:
        return render(request, 'blog/detail.html', context)
    else:
        context['error'] = 'Article does not exist.'
        print(context)
        return render(request, 'blog/detail.html', context)

def get_article_categories(request, category):
    articles = Article.objects.filter(categories__name=category, public=True)
    context = {
        'articles': articles
    }
    return render(request, 'blog/articles_categories.html', context)