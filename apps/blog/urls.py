from django.urls import path
from apps.blog.views import article_detail, get_article_categories, index


urlpatterns = [
    path('index/',index),
    path('detail/<str:slug>/', article_detail),
    path('categories/<str:category>/', get_article_categories),
]