django-tzuzul_project
Steps to use this code:

1) pip install -r requirements.txt
2) python manage.py makemigrations
3) python manage.py migrate
4) python manage.py createsuperuser
5) python manage.py runserver
    - Go to /admin
    - Login with your credentials created when run createsuperuser (username, password, not email and password)
    - Here you gotta register some Articles and Category
    - Finally, go to /index

======================
tailwind installation
1) python manage.py tailwind install
2) python manage.py tailwind start
